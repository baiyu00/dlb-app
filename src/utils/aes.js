import CryptoJS from 'crypto-js'

var keyHex = "shufenrd@dlb.com"

//加密
export function encryptByAES(message){
    return CryptoJS.AES.encrypt(message, CryptoJS.enc.Utf8.parse(keyHex), {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    }).toString();
  }

//解密
export function decryptByAES(encrypt){   //encrypt：加密结果
    return CryptoJS.AES.decrypt(encrypt, CryptoJS.enc.Utf8.parse(keyHex), {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
}