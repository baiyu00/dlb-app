// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import 'babel-polyfill'
import Es6Promise from 'es6-promise'
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'

import '@/styles/index.styl'
import '@/font/iconfont.css'

import VueCountUp from 'vue-countupjs'

import '@/permission' // permission control

Vue.use(MintUI)
Vue.use(VueCountUp)

Vue.config.productionTip = false

Es6Promise.polyfill()

// import Vconsole from 'vconsole'
// let vConsole = new Vconsole()

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
