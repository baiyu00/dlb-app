
const app = {
  state: {
    loading: false,
    readlist:[]
  },
  mutations: {
    LOADING_STATUS: (state, status) => {
      state.loading = status
    },
    READLIST_STATUS: (state, status) => {
      state.readlist.push(status)
    }
  },
  actions: {
    setLoadingState({ commit }, status) {
      commit('LOADING_STATUS', status)
    },
    setReadlistState({ commit }, status) {
      commit('READLIST_STATUS', status)
    }
  }
}

export default app
