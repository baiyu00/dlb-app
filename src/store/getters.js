const getters = {
    //公共
    loading: state => state.app.loading,
    readlist:state => state.app.readlist,

    //user 用户
    token: state => state.user.token,
    userInfo: state => state.user.userInfo,
    walletInfo: state => state.user.walletInfo
  }
  export default getters
  