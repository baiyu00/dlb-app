import Cookies from 'js-cookie'

export function getCookie(name) {
  return Cookies.get(name)
}

export function setCookie(name,value,expires) {
  if( expires ){
    return Cookies.set(name, value, { expires : expires})
  }else{
    return Cookies.set(name, value)
  }
}

export function removeCookie(name) {
  return Cookies.remove(name)
}

export function removeAllCookie() {
  Cookies.remove('comming')
  Cookies.remove('token')
  Cookies.remove('1')
  Cookies.set('relogin', true)   // 标记报错为4000系列重新登录动作
  return true;
}
