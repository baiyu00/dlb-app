import router from './router'
import store from './store'
import NProgress from 'nprogress' // Progress 进度条
import 'nprogress/nprogress.css'// Progress 进度条样式
import { getCookie } from '@/utils/auth'

router.beforeEach((to, from, next) => {
  NProgress.start()
  //判断是否登陆
  var isLogin = getCookie('token');
  //判断登录权限
  var isAuth = to.meta.isRequireAuthTrue;
  //判断是否是否为4000报错再登录
  var isReLogin = getCookie('relogin');

  if( isAuth ){
    if(isLogin){
      if(!store.getters.userInfo) {   //store里的用户信息数据为空时 要单独拉取数据
        store.dispatch('GetInfo').then(res => {    //拉取用户信息
          next()
        })
      }else {
        next()
      }
      if(!store.getters.walletInfo) {   //store里的钱包信息数据为空时 要单独拉取数据
        store.dispatch('GetWalletInfo').then(res => {    //拉取钱包信息
          next()
        })
      }else {
        next()
      }
    }else {
      next({path:'/login'}) //没有登陆
    }
  }else{
    if( to.name == "login" ){
      if(isLogin){
        next('/home');
      }else{
        next();
      }
    }else{
      next();
    }
  }
  NProgress.done()
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
