
const Mock = require('mockjs') // 获取mock对象
const Random = Mock.Random // 获取random对象，随机生成各种数据
const domain = 'http://mockjs.com/api' // 定义默认域名，随便写
const code = 200 // 返回的状态码

// 随机生成新闻列表数据
const getnewsList = function() {
    let array = [];
    for (let i = 0; i < 10; i++) {
        let item = {
            id:Random.csentence(5, 30),
            title: Random.csentence(5, 30), //  Random.csentence( min, max )
            imgSrc: Random.dataImage('300x250', 'mock的图片'), // Random.dataImage( size, text ) 生成一段随机的 Base64 图片编码
            time: Random.date() + ' ' + Random.time(), // Random.date()指示生成的日期字符串的格式,默认为yyyy-MM-dd；Random.time() 返回一个随机的时间字符串
            from: Random.cname()  // Random.cname() 随机生成一个常见的中文姓名
        }
        array.push(item)
    }
 
    return {
        array: array
    }
}


// 随机生成用户数据
const getuserList = function() {
    let object = {
        mobile: Random.cname(),
        id:Random.csentence(11),
        name: Random.csentence(1,10),
        src: Random.dataImage('300x250', 'mock的图片'),
        todayGotDLB:"5.235",
        totalDLB:"120.3692",
        usableDLB:"24.09",
        unlockDLB:"45.6",
        lockDLB:"19.654",
        todayReadTime:"48分钟",
        todayReadPage:"18",
        totalReadTime:"6小时24分钟",
        shareCode:"238963",
        loginPwd:false,
        payPwd:false
    }
    return {
        object: object
    }
}

 
// Mock.mock( url, post/get , 返回的数据)；
Mock.mock('/getnewslist', 'post', getnewsList);
Mock.mock('/getuserlist', 'post', getuserList);