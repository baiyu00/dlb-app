import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import Login from "@/views/Login";
import Register from "@/views/Register";
import Findpwd from "@/views/Findpwd/Findpwd";
import Home from "@/views/Home/Home";
import Order from "@/views/Order/Order";
import OrderNews from "@/views/Order/OrderNews";
import OrderNewsDetail from "@/views/Order/OrderNewsDetail";
import Account from "@/views/Account/Account";
import Wallet from "@/views/Account/Wallet";
import Usable from "@/views/Account/Wallet/Usable";
import Tocash from "@/views/Account/Wallet/Usable/tocash";
import Inputpwd from "@/views/Account/Wallet/Usable/inputpwd";
import Cashres from "@/views/Account/Wallet/Usable/cashres";
import Unlock from "@/views/Account/Wallet/Unlock";
import UnlockList from "@/views/Account/Wallet/UnlockList";
import Lock from "@/views/Account/Wallet/Lock";
import MyOrder from "@/views/Account/Wallet/Order";
import Invite from "@/views/Account/Invite";
import Setting from "@/views/Account/Setting";
import EditLoginPwd from "@/views/Account/Setting/editLoginPwd";
import EditPayPwd from "@/views/Account/Setting/editPayPwd";
import FindPayPwd from "@/views/Account/Setting/findPayPwd";
import EditPhone from "@/views/Account/Setting/editPhone";
import About from "@/views/Account/Setting/about";
import Help from "@/views/Account/Setting/help";
import Feedback from "@/views/Account/Setting/feedback";
import MyInfo from "@/views/Account/MyInfo";
import EditHeadImg from "@/views/Account/MyInfo/editHeadImg";
import EditNickname from "@/views/Account/MyInfo/editNickname";

export const constantRouterMap = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : false
    },
    // component: resolve => require(['@/views/Login'], resolve)
    component:Login
  },
  {
    path: '/register',
    name: 'register',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : false
    },
    // component: resolve => require(['@/views/Register'], resolve)
    component:Register
  },
  {
    path: '/findpwd',
    name: 'findpwd',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : false
    },
    // component: resolve => require(['@/views/Findpwd/Findpwd'], resolve),
    component: Findpwd
  },
  {
    path: '/home',
    name: 'home',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Home/Home'], resolve)
    component:Home
  },
  {
    path: '/order',
    name:'order',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Order/Order'], resolve)
    component: Order
  },
  {
    path: '/order/orderNews',
    name:'orderNews',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Order/OrderNews'], resolve)
    component: OrderNews
  },
  {
    path: '/order/orderNewsDetail',
    name:'orderNewsDetail',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Order/OrderNewsDetail'], resolve)
    component: OrderNewsDetail
  },
  {
    path: '/account',
    name: 'account',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Account'], resolve),
    component: Account
  },
  {
    path:'/account/wallet',
    name: 'wallet',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet'], resolve),
    component: Wallet
  },
  {
    path:'/account/wallet/usable',
    name: 'usable',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Usable'], resolve),
    component: Usable
  },
  {
    path:'/account/wallet/tocash',
    name: 'tocash',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Usable/tocash'], resolve),
    component: Tocash
  },
  {
    path:'/account/wallet/inputpwd',
    name: 'inputpwd',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Usable/inputpwd'], resolve),
    component: Inputpwd
  },
  {
    path:'/account/wallet/cashres',
    name: 'cashres',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Usable/cashres'], resolve),
    component: Cashres
  },
  {
    path:'/account/wallet/unlock',
    name: 'unlock',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Unlock'], resolve),
    component: Unlock
  },
  {
    path:'/account/wallet/unlocklist',
    name: 'unlocklist',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/UnlockList'], resolve),
    component: UnlockList
  },
  {
    path:'/account/wallet/lock',
    name: 'lock',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Lock'], resolve),
    component: Lock
  },
  {
    path:'/account/wallet/order',
    name: 'myorder',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Wallet/Order'], resolve),
    component: MyOrder
  },
  {
    path:'/account/invite',
    name: 'invite',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Invite'], resolve),
    component: Invite
  },
  {
    path:'/account/setting',
    name: 'setting',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting'], resolve),
    component: Setting
  },
  {
    path:'/account/setting/editLoginPwd',
    name: 'editLoginPwd',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/editLoginPwd'], resolve),
    component: EditLoginPwd
  },
  {
    path:'/account/setting/editPayPwd',
    name: 'editPayPwd',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/editPayPwd'], resolve),
    component: EditPayPwd
  },
  {
    path:'/account/setting/editPayPwd/findPayPwd',
    name: 'findPayPwd',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/findPayPwd'], resolve),
    component: FindPayPwd
  },
  {
    path:'/account/setting/editPhone',
    name: 'editPhone',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/editPhone'], resolve),
    component:EditPhone
  },
  {
    path:'/account/setting/about',
    name: 'about',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/about'], resolve),
    component: About
  },
  {
    path:'/account/setting/help',
    name: 'help',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/help'], resolve),
    component: Help
  },
  {
    path:'/account/setting/feedback',
    name: 'feedback',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/Setting/feedback'], resolve),
    component: Feedback
  },
  {
    path:'/account/myInfo',
    name: 'myInfo',
    meta: {
      keepAlive: false,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/MyInfo'], resolve),
    component: MyInfo
  },
  {
    path:'/account/myInfo/editHeadImg',
    name: 'editHeadImg',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/MyInfo/editHeadImg'], resolve),
    component: EditHeadImg
  },
  {
    path:'/account/myInfo/editNickname',
    name: 'editNickname',
    meta: {
      keepAlive: true,
      isRequireAuthTrue : true
    },
    // component: resolve => require(['@/views/Account/MyInfo/editNickname'], resolve),
    component: EditNickname
  }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior (to, from, savedPosition) {
    if( to.name == 'home' || to.name == 'orderNews' ){
      return savedPosition
    }else{
      return { x: 0, y: 0 }
    }
  },
  routes: constantRouterMap
})
