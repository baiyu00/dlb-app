'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // API_ROOT: '"http://192.168.10.170:8082/v1"'
  // API_ROOT: '"http://23y4493q98.51mypc.cn:11985/v1/"'
  API_ROOT: '"http://47.75.99.213:8082/v1"'
})
