import { login, getInfo } from '@/api/login'
import { updateInfo, updateInfoByHeadImg, GetWallet } from '@/api/api'
import { getCookie, setCookie, removeAllCookie } from '@/utils/auth'
import { decryptByAES } from '@/utils/aes'

const user = {
  state: {
    token: getCookie('token'),
    userInfo : null,
    walletInfo: null
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USERINFO: (state, info) => {
      state.userInfo = info
    },
    SET_WALLETINFO: (state, info) => {
      state.walletInfo = info
    }
  },

  actions: {
    //刷新token
    resetToken({ commit }, status) {
      commit('SET_TOKEN', status)
    },

    // 登录
    Login({ commit }, params) {
      return new Promise((resolve, reject) => {
        login(params).then(response => {
          const data = JSON.parse(decryptByAES(response.data));
          setCookie('token','dlb '+ data.token,100)
          commit('SET_TOKEN', "dlb "+ data.token)
          response.data = data;
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({ commit }) {
      return new Promise((resolve) => {
        commit('SET_TOKEN', '')
        removeAllCookie()
        resolve()
      })
    },

    // 获取用户信息
    GetInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          if( response.data ){
            commit('SET_USERINFO', response.data);
          }
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取钱包信息
    GetWalletInfo({ commit }) {
      return new Promise((resolve, reject) => {
        GetWallet().then(response => {
          if( response.data ){
            const data = JSON.parse(decryptByAES(response.data));
            commit('SET_WALLETINFO',data)
            response.data = data;
          }
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    //修改用户信息
    UpdateInfo({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        updateInfo(userInfo).then(response => {
          const data = JSON.parse(decryptByAES(response.data));
          commit('SET_USERINFO', {
            name: data.name,
            photo: data.photo,
            mobile:data.mobile,
            loginPassword:data.loginPassword,
            payPassword:data.payPassword
          })
          response.data = data;
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    //修改用户信息--头像
    UpdateInfoByHeadImg({ commit }, file) {
      return new Promise((resolve, reject) => {
        updateInfoByHeadImg(file).then(response => {
          const data = response.data
          commit('SET_USERINFO', {
            photo: data.photo,
          })
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
  }
}

export default user
