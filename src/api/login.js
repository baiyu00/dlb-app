import {encryptByAES} from '@/utils/aes'
import request from '@/utils/request'

export function login(params) {
  return request({
    url: '/user/login',
    method: 'post',
    data:{
      "data":encryptByAES(JSON.stringify(params))
    }
  })
}

export function getInfo() {
  return request({
    url: '/user/getUser',
    method: 'post',
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
