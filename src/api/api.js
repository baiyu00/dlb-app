import {encryptByAES} from '@/utils/aes'
import request from '@/utils/request'

// 获取验证码
export function getVerificationCode(params) {
    return request({
        url: '/sms/getVerificationCode',
        method: 'post',
        data: params
    })
}

//刷新token
export function refreshToken() {
    return request({
        url: '/user/refreshToken',
        method: 'post'
    })
}

// 注册
export function requestRegister(params) {
    return request({
        // url: '/user/register',
        url:'/sys/registerPub',   //公测期间的注册接口
        method: 'post',
        data: params
    })
}

// 找回登录密码
export function UpdateLoginPassword(params) {
    return request({
        url: '/user/updateLoginPassword',
        method: 'post',
        data:{
            "data":encryptByAES(JSON.stringify(params))
        }
    })
}

// 找回支付密码
export function UpdatePayPassword(params) {
    return request({
        url: '/user/updatePayPassword',
        method: 'post',
        data:{
            "data":encryptByAES(JSON.stringify(params))
        }
    })
}

// 获取新闻列表
export function getNewsList(params) {
    return request({
        url: '/new/list',
        method: 'post',
        data: params
    })
}

// 获取新闻分类
export function getNewsOrderList() {
    return request({
        url: '/new/class',
        method: 'get'
    })
}

// 获取新闻详情
export function getNewsInfo(params) {
    return request({
        url: '/new/info/'+ params.newId,
        method: 'get'
    })
}

// 上传新闻阅读的时间
export function postNewsRead(params) {
    return request({
        url: '/new/read',
        method: 'post',
        data: params
    })
}

// 领取dlb
export function getDlb(params) {
    return request({
        url: '/new/getdlb',
        method: 'post',
        data:{
            "data":encryptByAES(JSON.stringify(params))
        }
    })
}

// 修改用户信息
export function updateInfo(params) {
    return request({
        url: '/user/update',
        method: 'post',
        data:{
            "data":encryptByAES(JSON.stringify(params))
        }
    })
}

// 修改用户信息--头像
export function updateInfoByHeadImg(params) {
    return request({
        url: '/user/photo',
        method: 'post',
        data:params
    })
}

// 上传反馈信息
export function PostUserFeedback(params) {
    let formData = new FormData;
    formData.append('file', params.file);
    formData.append("contactMobile", params.contactMobile);
    formData.append("content", params.content);
    return request({
        url: '/user/userFeedback',
        method: 'post',
        data:formData
    })
}

// 获取钱包信息
export function GetWallet() {
    return request({
        url: '/user/wallet',
        method: 'post'
    })
}

// 解锁
export function PostUnlock(params) {
    return request({
        url: '/wallet/unlock',
        method: 'post',
        data:{
            "data":encryptByAES(JSON.stringify(params))
        }
    })
}

// 取消解锁
export function PostCancelLock(params) {
    return request({
        url: '/wallet/cancelLock',
        method: 'post',
        data:params
    })
}

// 解锁列表
export function GetUnlockList(params) {
    return request({
        url: '/wallet/unlockList',
        method: 'post',
        data:params
    })
}

// 提现
export function PostExtract(params) {
    return request({
        url: '/wallet/extract',
        method: 'post',
        data:{
            "data":encryptByAES(JSON.stringify(params))
        }
    })
}

// 提现列表
export function GetExtractList(params) {
    return request({
        url: '/wallet/extractList',
        method: 'post',
        data:params
    })
}

// 获取app版本
export function GetAppVersion(params) {
    return request({
        url: '/sys/appVersion/'+ params.systemType,
        method: 'get'
    })
}
